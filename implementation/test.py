STRAIGHT = b'!Ce\x87\x00\x00\x00\x00\x00\x00'
OPEN = b'\x00\x00\x00\x00\x00\x00\x00\x00\x00\x00'
LOOPBACK = b'\x00\x00\x00\x00cp\x08\x00\x00\x00'
PORT = b'\x00\x00\x00\x00\x02\x11\x11\x11\x00\x00'

data = STRAIGHT

connections = [0, 0, 0, 0, 0, 0, 0, 0, 0]
shorts = [0, 0, 0, 0, 0, 0, 0, 0, 0]
for i in range(0, 4):
    a = (i * 2) + 1
    connections[a] = data[i] & 15
    shorts[a] = data[i + 4] & 15
    b = (i * 2) + 2
    connections[b] = (data[i] & 240) >> 4
    shorts[b] = (data[i + 4] & 240) >> 4

connections[0] = data[8] & 15
shorts[0] = (data[8] & 240) >> 4

print(connections)
print(shorts)
